package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	usage = `
	___  ____ _    _    _ _  _ ____ ___ ____ ____ 
	|__] |  | |    |    | |\ | |__|  |  |  | |__/ 
	|    |__| |___ |___ | | \| |  |  |  |__| |  \   (%s)
	
Description:
    Move music torrents from one Gazelle tracker to another.
	
Usage:
    pollinator <TRACKER_ORIGIN> <TRACKER_DEST> <ORIGIN_ID_PL_OR_TORRENT>
    pollinator show-config

Options:
    -h, --help             Show this screen.
    --version              Show version.
`
	fullName    = "pollinator"
	fullVersion = "%s -- %s"
)

var Version = "dev"

func userAgent() string {
	return fullName + "/" + Version
}

type pollinatorArgs struct {
	builtin            bool
	showConfig         bool
	trackerOrigin      string
	trackerDestination string
	torrentID          int
	torrentPath        string
}

func (m *pollinatorArgs) parseCLI(osArgs []string) error {
	// parse arguments and options
	args, err := docopt.Parse(fmt.Sprintf(usage, Version), osArgs, true, fmt.Sprintf(fullVersion, fullName, Version), false, false)
	if err != nil {
		return errors.Wrap(err, "incorrect arguments")
	}
	if len(args) == 0 {
		// builtin command, nothing to do.
		m.builtin = true
		return nil
	}
	m.showConfig = args["show-config"].(bool)
	if m.showConfig {
		return nil
	}

	m.trackerOrigin = args["<TRACKER_ORIGIN>"].(string)
	m.trackerDestination = args["<TRACKER_DEST>"].(string)

	idString := args["<ORIGIN_ID_PL_OR_TORRENT>"].(string)
	r := regexp.MustCompile(`^.*/torrents\.php\?torrentid=(\d+)$`)
	if torrentID, err := strconv.Atoi(idString); err != nil {
		// checking if path
		if fs.FileExists(idString) && (strings.HasSuffix(idString, ".torrent") || strings.HasSuffix(idString, ".torrent.added")) {
			m.torrentPath = idString
		} else if hits := r.FindStringSubmatch(idString); len(hits) != 0 {
			m.torrentID, _ = strconv.Atoi(hits[1])
		} else {
			return errors.New("invalid torrent ID or path")
		}
	} else {
		m.torrentID = torrentID
	}
	return nil
}
