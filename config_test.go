package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfig(t *testing.T) {
	fmt.Println("+ Testing Config...")
	check := assert.New(t)

	// setting up
	check.Nil(os.Mkdir("test/watch", 0777))
	check.Nil(os.Mkdir("test/download", 0777))
	defer os.Remove("test/watch")
	defer os.Remove("test/download")

	c := &Config{}
	err := c.Load("test/config.yaml")
	check.Nil(err)

	// general
	fmt.Println("Checking general")
	check.Equal(3, c.General.LogLevel)

	// trackers
	fmt.Println("Checking trackers")
	check.Equal(2, len(c.Trackers))
	tr := c.Trackers[0]
	check.Equal("blue", tr.Name)
	check.Equal("this_guy", tr.User)
	check.Equal("uniquepasswordforsecurity", tr.Password)
	check.Equal("eoifdoijheRFrs30493eoifdoijheRFrs30493", tr.Cookie)
	check.Equal("https://blue.it", tr.URL)
	tr = c.Trackers[1]
	check.Equal("purple", tr.Name)
	check.Equal("this_guy_s_alter_ego", tr.User)
	check.Equal("uniquepasswordforsecurity", tr.Password)
	check.Equal("eoifdoipoipoipoipoipoipoiFrs304poi6593", tr.Cookie)
	check.Equal("https://purpl.es", tr.URL)

	// folders
	fmt.Println("Checking folders")
	check.Equal("test/watch", c.Folders.WatchDir)
	check.Equal("test/download", c.Folders.DownloadDir)

	fmt.Println(c.String())
}
